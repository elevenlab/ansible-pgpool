# %d = node ID of the detached node
# %h = hostname of detached
# %p = port number of detached
# %D = database dir of detached
# %H = hostname of new master
# %r = port number of new master

detached_id=$1
detached_host=$2
#detached_db_port=$3
#detached_data_dir=$4
#primary_host=$5
#primary_db_port=$6

ssh -T postgres@$detached_host pg_ctlcluster {{ pg_version }} {{ pg_cluster_name }} stop -m fast

usr/sbin/pcp_recovery_node -w -h localhost -p {{ pgpool_pcp_port }} $detached_id

#echo "ssh -T postgres@$primary_host \"psql -p $primary_db_port -c \"SELECT pgpool_recovery('basebackup.sh', '$detached_host','$detached_data_dir', '$detached_db_port')\" template1\""
#ssh -T postgres@$primary_host "psql -p $primary_db_port -c \"SELECT pgpool_recovery('basebackup.sh', '$detached_host','$detached_data_dir', '$detached_db_port')\" template1"

#echo "ssh -T postgres@$primary_host \"psql -p $primary_db_port -c \"SELECT pgpool_remote_start('$detached_host','$detached_data_dir')\" template1\""
#ssh -T postgres@$primary_host "psql -p $primary_db_port -c \"SELECT pgpool_remote_start('$detached_host','$detached_data_dir')\" template1"
