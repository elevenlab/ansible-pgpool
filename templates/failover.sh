#!/bin/sh -x
# Execute command by failover.
# special values:  %d = node id of detached node
#                  %h = hostname of detached node
#                  %p = port number of detached node
#                  %D = database cluster path of detached node
#                  %m = new master node id (master means the node alive with the smallest id)
#                  %M = old master node id
#                  %H = new master node host name
#                  %P = old primary node id (primary is the node with database write capabilities - the master in master/slave)
#                  %r = Port number of the new master node
#                  %R = database cluster path of new master node
#                  %% = '%' character
failed_node_id=$1
failed_host_name=$2
failed_port=$3
failed_db_path=$4
new_master_id=$5
old_master_id=$6
new_master_host_name=$7
old_primary_node_id=$8
new_master_port=$9
new_master_db_path=$10
#if [ $new_master_db_path != '*/' ];then
#    new_master_db_path=$new_master_db_path/
#fi

trigger=$new_master_db_path"trigger_file"

UID=$(id -u)

if [ $failed_node_id = $old_primary_node_id ];then	# master failed
    if [ $UID -eq 0 ]
    then
        su postgres -c "ssh -T postgres@$new_master_host_name touch $trigger"        # let standby take over
    else
        ssh -T postgres@$new_master_host_name touch $trigger	# let standby take over
    fi
    
    #create recovery.conf to use in recovery_1st_stage
    tmp=/tmp/recovery_temp$$
    trap "rm -f $tmp" 0 1 2 3 15

    cat > $tmp <<EOF
standby_mode          = 'on'
primary_conninfo      = 'user={{pg_replication_user}} password={{pg_replication_password}} host=$new_master_host_name port=$new_master_port'
trigger_file = '$trigger'
EOF
    
    
    scp $tmp $new_master_host_name:$new_master_db_path"recovery.template"
fi



